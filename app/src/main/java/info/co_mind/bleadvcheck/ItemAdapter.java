package info.co_mind.bleadvcheck;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mugi on 2015/12/15.
 */
public class ItemAdapter extends BaseAdapter {
    public static final int ITEM_STATE_NORMAL = 0;
    public static final int ITEM_TITLE = 1;
    public static final int ITEM_STATE_TRUE = 2;
    public static final int ITEM_STATE_FALSE = 3;
    public static final int ITEM_STATE_NOT_SUPPORT = 4;

    private final Context context;
    private final LayoutInflater inflater;
    private class Row {
        String title;
        String value;
        int type;

        public Row (String title, String value, int type) {
            this.title = title;
            this.value = value;
            this.type = type;
        }
    }

    private ArrayList<Row> rows = new ArrayList<Row>();

    public ItemAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void addItem(String title, String value, int type) {
        rows.add(new Row(title, value, type));
    }

    public void addItem(String title, String value) {
        rows.add(new Row(title, value, ITEM_STATE_NORMAL));
    }

    public void clear() { rows.clear(); }

    @Override
    public int getCount() { return rows.size(); }

    @Override
    public Object getItem(int i) { return rows.get(i); }

    @Override
    public long getItemId(int i) { return i; }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;

        Row row = rows.get(i);
        if (view == null) {
//            if (row.type == ITEM_TITLE) {
//                view = viewTitle;
//            } else {
//                view = viewNormal;
//            }
//            view.setTag(viewHolder);
            viewHolder = new ViewHolder();
            view = inflater.inflate(R.layout.li_item, null);
            viewHolder.itemValue = (TextView) view.findViewById(R.id.item_value);
            viewHolder.itemTitle = (TextView) view.findViewById(R.id.item_title);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.itemTitle.setText(row.title);
        viewHolder.itemValue.setText(row.value);

        switch (row.type) {
            case ITEM_TITLE:
                view.setBackgroundColor(Color.parseColor("#3F51B5"));
                viewHolder.itemTitle.setTextColor(Color.WHITE);
                viewHolder.itemValue.setVisibility(View.GONE);
                break;
            case ITEM_STATE_TRUE:
                view.setBackgroundColor(Color.GREEN);
                viewHolder.itemTitle.setTextColor(Color.BLACK);
                viewHolder.itemValue.setVisibility(View.VISIBLE);
                break;
            case ITEM_STATE_FALSE:
                view.setBackgroundColor(Color.RED);
                viewHolder.itemTitle.setTextColor(Color.BLACK);
                viewHolder.itemValue.setVisibility(View.VISIBLE);
                break;
            case ITEM_STATE_NORMAL:
                view.setBackgroundColor(Color.WHITE);
                viewHolder.itemTitle.setTextColor(Color.BLACK);
                viewHolder.itemValue.setVisibility(View.VISIBLE);
                break;
            case ITEM_STATE_NOT_SUPPORT:
                view.setBackgroundColor(Color.parseColor("#404040"));
                viewHolder.itemTitle.setTextColor(Color.WHITE);
                viewHolder.itemValue.setVisibility(View.GONE);
                break;
        }
        return view;
    }

    private static class ViewHolder {
        TextView itemTitle;
        TextView itemValue;
    }
}
