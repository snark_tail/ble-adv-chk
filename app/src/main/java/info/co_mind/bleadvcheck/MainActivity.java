package info.co_mind.bleadvcheck;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.view.LayoutInflater;

import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private LayoutInflater inflater;
    private ItemAdapter itemListAdapter;
    private ItemAdapter adapter;
    private String bluetoothStatus;     // コピペ用のメッセージ
    private String deviceStatus;        // コピペ用のメッセージ

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Build build = new Build();
        Build.VERSION buildVersion =  new Build.VERSION();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 画面上部のバーの表示
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        // リストビューにデータを設定
        ListView listView1 = (ListView)findViewById(R.id.listView1);

        adapter = new ItemAdapter(MainActivity.this);
        listView1.setAdapter(adapter);

        BluetoothManager bluetoothManager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter mBluetoothAdapter = bluetoothManager.getAdapter();
        bluetoothStatus = "Bluetooth Status" + "\n";
        if (null == mBluetoothAdapter) {
            adapter.addItem("Bluetooth is NOT supported", "", ItemAdapter.ITEM_STATE_NOT_SUPPORT);
            bluetoothStatus += "Bluetooth is NOT supported";
        } else {
            adapter.addItem("Advertise Function Status", "", ItemAdapter.ITEM_TITLE);
            setFunctionItem(mBluetoothAdapter.isMultipleAdvertisementSupported(), "isMultipleAdvertisementSupported()");
            setFunctionItem(mBluetoothAdapter.isOffloadedFilteringSupported(), "isOffloadedFilteringSupported()");
            setFunctionItem(mBluetoothAdapter.isOffloadedScanBatchingSupported(), "isOffloadedScanBatchingSupported()");
            bluetoothStatus +=
                    getFunctionString(mBluetoothAdapter.isMultipleAdvertisementSupported(), "isMultipleAdvertisementSupported()") + "\n"+
                    getFunctionString(mBluetoothAdapter.isOffloadedFilteringSupported(), "isOffloadedFilteringSupported()") +  "\n"+
                    getFunctionString(mBluetoothAdapter.isOffloadedScanBatchingSupported(), "isOffloadedScanBatchingSupported()") + "\n"
                ;
        }

        adapter.addItem("About phone", "", ItemAdapter.ITEM_TITLE);
        adapter.addItem("Model number", build.MODEL);
        adapter.addItem("Android version", buildVersion.RELEASE);
        adapter.addItem("Build number", build.DISPLAY);

        deviceStatus = "Devaice Status" + "\n" +
                "Model number:" + build.MODEL + "\n" +
                "Android version:" + buildVersion.RELEASE + "\n" +
                "Build number:" + build.DISPLAY
            ;
        adapter.addItem("android.os.Build", "", ItemAdapter.ITEM_TITLE);
        adapter.addItem("BOARD", build.BOARD);
        adapter.addItem("BOOTLOADER", build.BOOTLOADER);
        adapter.addItem("BRAND", build.BRAND);
        adapter.addItem("CPU_ABI", build.CPU_ABI);
        adapter.addItem("CPU_ABI2", build.CPU_ABI2);
        adapter.addItem("DEVICE", build.DEVICE);
        adapter.addItem("DISPLAY", build.DISPLAY);
        adapter.addItem("FINGERPRINT", build.FINGERPRINT);
        adapter.addItem("HARDWARE", build.HARDWARE);
        adapter.addItem("HOST", build.HOST);
        adapter.addItem("ID", build.ID);
        adapter.addItem("MANUFACTURER", build.MANUFACTURER);
        adapter.addItem("MODEL", build.MODEL);
        adapter.addItem("PRODUCT", build.PRODUCT);
        adapter.addItem("RADIO", build.RADIO);
        adapter.addItem("SERIAL", build.SERIAL);

        adapter.addItem("SUPPORTED_32_BIT_ABIS", readStringArray(build.SUPPORTED_32_BIT_ABIS));
        adapter.addItem("SUPPORTED_64_BIT_ABIS", readStringArray(build.SUPPORTED_64_BIT_ABIS));
        adapter.addItem("SUPPORTED_ABIS", readStringArray(build.SUPPORTED_ABIS));
        adapter.addItem("TAGS", build.TAGS);
        Date currDate = new Date(build.TIME);
        adapter.addItem("TIME", currDate.toString());
        adapter.addItem("TYPE", build.TYPE);
        adapter.addItem("USER", build.USER);

        adapter.addItem("android.os.Build.VERSION", "", ItemAdapter.ITEM_TITLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            adapter.addItem("BASE_OS", buildVersion.BASE_OS);
        }
        adapter.addItem("CODENAME", buildVersion.CODENAME);
        adapter.addItem("INCREMENTAL", buildVersion.INCREMENTAL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            adapter.addItem("PREVIEW_SDK_INT", String.valueOf(buildVersion.PREVIEW_SDK_INT));
        }
        adapter.addItem("RELEASE", buildVersion.RELEASE);
        adapter.addItem("SDK", buildVersion.SDK);
        adapter.addItem("SDK_INT", String.valueOf(buildVersion.SDK_INT));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            adapter.addItem("SECURITY_PATCH", buildVersion.SECURITY_PATCH);
        }

        adapter.notifyDataSetChanged();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.copy_to_clipboard:
                String Result = bluetoothStatus + "\n" + deviceStatus;
                Log.d("AppBar", "Result");
                //クリップボードに格納するItemを作成
                ClipData.Item text = new ClipData.Item(Result);

                //MIMETYPEの作成
                String[] mimeType = new String[1];
                mimeType[0] = ClipDescription.MIMETYPE_TEXT_PLAIN;

                //クリップボードに格納するClipDataオブジェクトの作成
                ClipData cd = new ClipData(new ClipDescription("text_data", mimeType), text);

                //クリップボードにデータを格納
                ClipboardManager cm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                cm.setPrimaryClip(cd);
                return true;
//
//            case R.id.action_favorite:
//                // User chose the "Favorite" action, mark the current item
//                // as a favorite...
//                return true;
//
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
    private void setFunctionItem(boolean support, String title) {
        int type = ItemAdapter.ITEM_STATE_FALSE;
        String stateMsg = "false";
        if (support) {
            type = ItemAdapter.ITEM_STATE_TRUE;
            stateMsg = "true";
        }
        adapter.addItem(title, stateMsg, type);
    }

    private String getFunctionString(boolean support, String title) {
        if (support) {
            title = "true :" + title;
        } else {
            title = "false:" + title;
        }
        return title;
    }

    private String readStringArray(String [] source) {
        if (source.length == 0) {
            return "(no data)";
        }
        String destination = "";
        for (String string : source) {
            destination += string + "\n";
        }
        return destination.substring(0, destination.length() - 1);
    }
}
